import trae from 'trae'

const autosService = trae.create({
  //baseUrl: 'https://mejorsegurodeauto.mx/ws-autos/servicios'
  //baseUrl: 'http://192.168.34.156:8096/v1/camiones'
  //baseUrl: 'http://catalogoscamiones-develop.us-east-2.elasticbeanstalk.com/v1/camiones'
  baseUrl: 'https://ws-camiones.com/v1/camiones'
})

export default autosService



