import axios from "axios";
import configDB from "./configBase";

const cotizacionService = {};

cotizacionService.search = function(
  nombreUsr,
  telefonoUsr,
  emailUsr,
  generoUsr,
  codigoPostalUsr,
  edadUsr,
  datosCot,
  respuestaCot,
  emailValid,
  telefonoValid,
  mensajeSMS,
  codigoPostalValid,
  idSubRamo,
  idMedioDifusion,
  aseguradora,
  idPagina,
  ip,
  telefonoAS,
  accessToken
) {
  return axios({
    method: "post",
    headers: { Authorization: `Bearer ${accessToken}` },
    //TRUCK-TRACT CAMIONES
    url: process.env.promoCore+  '/v1/truck-tract', 
    data: {
      nombreUsr,
      telefonoUsr,
      emailUsr,
      generoUsr,
      codigoPostalUsr,
      edadUsr,
      datosCot,
      respuestaCot,
      emailValid,
      telefonoValid,
      mensajeSMS,
      codigoPostalValid,
      idSubRamo,
      idMedioDifusion,
      aseguradora,
      idPagina,
      ip,
      telefonoAS,
    },
  });
};
export default cotizacionService;
