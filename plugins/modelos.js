import autosService from './ws-tractos'

const modelosService ={}

modelosService.search=function (idMarca) {
  return autosService.get('/camiones/modelos/'+idMarca,{
    params:{}
  })
    .then(res => res.data)
    .catch(err => console.error(err));
}
export default modelosService
