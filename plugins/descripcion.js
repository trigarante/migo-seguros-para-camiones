import autosService from './ws-tractos'

const descripsService={}

descripsService.search=function (idVersion) {
    return autosService.get('/camiones/descripcion/'+idVersion, {
        params: {}
    })
        .then(res => res.data)
        .catch(err => console.error(err))
}
export default descripsService