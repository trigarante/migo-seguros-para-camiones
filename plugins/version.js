import autosService from './ws-tractos'

const versionesService ={}

versionesService.search=function (idTipo) {
    return autosService.get('/camiones/version/'+idTipo,{
        params:{}
    })
        .then(res => res.data)
        .catch(err => console.error(err));
}
export default versionesService