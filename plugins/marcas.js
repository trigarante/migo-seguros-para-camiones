import autosService from './ws-tractos'

const marcasService ={}

marcasService.search=function (accessToken) {
  return autosService.get('/camiones/marcas',{
    headers: { Authorization: `Bearer ${accessToken}` },
    params:{}
  })
    .then(res => res.data)
    .catch(err => console.error(err));
}
export default marcasService
