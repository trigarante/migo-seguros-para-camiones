import trae from 'trae'

const autosService = trae.create({
  baseUrl: process.env.urlTactos+'/v1',
})

export default autosService