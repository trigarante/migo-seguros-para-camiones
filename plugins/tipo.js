import autosService from './ws-tractos'

const tiposService ={}

tiposService.search=function (idModelo) {
    return autosService.get('/camiones/tipo/'+idModelo,{
        params:{}
    })
        .then(res => res.data)
        .catch(err => console.error(err));
}
export default tiposService