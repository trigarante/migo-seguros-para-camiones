import Vuex from "vuex";
import validacionesService from "../plugins/validaciones";
import database from "~/plugins/saveData";
import dbService from "~/plugins/configBase";
import saveDataHubspot from "~/plugins/saveHubspot";
import getTokenService from "~/plugins/getToken";
import cotizacionPromo from "~/plugins/descuentoService.js";
import telApi from "~/plugins/telefonoService.js";
import saveDataCliente from "~/plugins/saveDataCliente";



const createStore = () => {
  const validacion = new validacionesService();
  return new Vuex.Store({
    state: {
      ambientePruebas: false,
      cargandocotizacion: false,
      msj: false,
      cotizacionesAmplia: [],
      cotizacionesLimitada: [],
      config: {
        time:'',
        loading:true,
        aseguradora: "",
        cotizacion: true,
        emision: false,
        descuento: 30,
        telefonoAS: "88902366",
        grupoCallback: "VN Agregadores",
        from: "DOMINIO-MIGO",
        idPagina: 0,
        asgNombre: "Migo Seguros",
        dominio: "migoseguros.com",
        accessToken: "",
        k1: "",
        k2: "",
        k3: "",
        k4: "",
        k5: "",

        desc:'',
        msi:'',
        promoImg:'',
        promoLabel:'',
        promoSpecial:'false',
      },
      ejecutivo: {
        nombre: "",
        correo: "",
        id: 0,
      },
      formData: {
        idLogData: '',
        idHubspot: '',
        urlOrigen: "",
        aseguradora: "",
        marca: "",
        idMarca: "",
        idModelo: "",
        idDescripcion: "",
        idSubDescripcion: "",
        idDetalles: "",
        modelo: "",
        descripcion: "",
        subDescripcion: "",
        detalle: "",
        clave: "",
        cp: "",
        nombre: "",
        apellidoPaterno: "",
        apellidoMaterno: "",
        telefono: "",
        correo: "",
        edad: "",
        fechaNacimiento: "",
        genero: "",
        precio: "",
      },
      solicitud: {},
      cotizacion: {},
      servicios: {
        servicioDB: "http://138.197.128.236:8081/ws-autos/servicios",
      },
      prospecto: {
        numero: "",
        correo: "",
        nombre: "",
        sexo: "",
        edad: "",
      },
      productoSolicitud: {
        idProspecto: "",
        idTipoSubRamo: 1,
        datos: "",
      },
      cotizaciones: {
        idProducto: "",
        idPagina: "",
        idMedioDifusion: "",
        idEstadoCotizacion: 1,
        idTipoContacto: 1,
      },
      cotizacionAli: {
        idCotizacion: "",
        idSubRamo: "",
        peticion: "",
        respuesta: "",
      },
      solicitudes: {
        idCotizacionAli: "",
        idEmpleado: "",
        idEstadoSolicitud: 1,
        idEtiquetaSolicitud: 1,
        idFlujoSolicitud: 1,
        comentarios: "",
      },
    },
    actions: {
      getToken(state) {
        return new Promise((resolve, reject) => {
          getTokenService.search(dbService.tokenData).then(
            (resp) => {
              if (typeof resp.data != "undefined") {
                state.config.accessToken = resp.data.accessToken;
              } else {
                // state.config.accessToken = resp.accessToken;
                this.state.config.accessToken = resp.accessToken;
              }
              // localStorage.setItem("authToken", state.config.accessToken);
              localStorage.setItem("authToken", this.state.config.accessToken);
              resolve(resp);
            },
            (error) => {
              reject(error);
            }
          );
        });
      },
    },
    mutations: {

      enhancedConversionGTAG: function (state) {
        dataLayer.push({
          'event': 'Lead',
          'phone': '52' + state.formData.telefono,
          'email': state.formData.correo,
        });
      },
      saveDataCliente: function (state) {
        state.formData.dominioCorreo = state.config.dominioCorreo;
        let peticionProspecto = {
          nombreUsr: state.formData.nombre,
          telefonoUsr: state.formData.telefono,
          emailUsr: state.formData.correo,
          generoUsr: state.formData.genero,
          codigoPostalUsr: state.formData.cp,
          edadUsr: state.formData.edad,
          respuestaCot: "{}",
          emailValid: state.formData.emailValid === true ? 1 : 0,
          telefonoValid: state.formData.telefonoValid,
          mensajeSMS: state.config.textoSMS,
          codigoPostalValid: state.formData.codigoPostalValid === true ? 1 : 0,
          idSubRamo: state.config.idSubRamo,
          idMedioDifusion: state.config.idMedioDifusion,
          aseguradora: state.config.aseguradora,
          idPagina: state.config.idPagina,
          telefonoAS: state.config.telefonoAS
        };
        state.config.dataPeticionCotizacion = peticionProspecto;
        let datosCliente;
        state.formData.telefonoValid = false;
        datosCliente = {
          datosCot: JSON.stringify(state.formData),
          idMedioDifusion: state.config.idMedioDifusion,
          idPagina: state.config.idPagina,
          idSubRamo: state.config.idSubRamo,
          peticionesAli: JSON.stringify({}),
        };
        this.commit("enhancedConversionGTAG");
        saveDataCliente
          .search(
            JSON.stringify(datosCliente),
            state.config.accessToken
          )
          .then((resp) => {
           //idLogData salesForce
           state.formData.idLogData =resp.data.data.idLogClient
           this.commit("saveData");
          })
          .catch((error) => {
            var status = "ERR_CONNECTION_REFUSED";
            if (typeof error.status != "undefined") {
              status = error.status;
            } else if (typeof error.response != "undefined") {
              if (typeof error.response.status != "undefined") {
                status = error.response.status;
              }
            }
            if (status === 401) {
              this.$store.dispatch("getToken")
                .then((resp) => {
                  if (typeof resp.data == "undefined") {
                    state.config.accessToken = resp.accessToken;
                  } else if (typeof resp.data.accessToken != "undefined") {
                    state.config.accessToken = resp.data.accessToken;
                  }
                  // this.saveDataCliente();
                  this.commit("saveData");
                })
                .catch((error) => {
                  console.log("Hubo un problema al generar el token");
                  state.idEndPoint = 1;
                 
  
                });
            } else {
              state.idEndPoint = 1;
             
  
            }
          });
        //delete (state.formData.urlOrigen);
      },
      
      cotizacionPromo: function (state) {
        let aseguradora;
        if(state.config.aseguradora=="ABA"){
          aseguradora="CHUBB"
        }else{
          aseguradora=state.config.aseguradora;
        }
        cotizacionPromo
          .search(aseguradora)
          .then(resp => {
            resp = resp.data;
                    if(parseInt(resp.discount )){
                        state.config.descuento = resp.discount
                    }else{
                        state.config.descuento = 0
                    }

            }).catch(error=>{
                        state.config.descuento = 0
                    });

        },
        
      telApi: function (state) {     
       try {
         telApi
         .search(state.config.idMedioDifusion)
         .then(resp => {
           resp = resp.data;
           let telefono=resp.telefono;
           if(parseInt(telefono)){
             var tel = telefono +""
             var tel2= tel.substring(2,tel.length)
             console.log(tel2);
             state.config.telefonoAS = tel2
           }
         });
         } catch (error) {
         console.log(error);
         } 
      },
      
     
      validarTokenCore: function(state) {
        try {
          if (process.browser) {
            if (
              localStorage.getItem("authToken") === null ||
              localStorage.getItem("authToken") === "undefined"
            ) {
              state.sin_token = true;
              console.log("NO HAY TOKEN...");
            } else {
              console.log("VALIDANDO TOKEN...");
              state.config.accessToken = localStorage.getItem("authToken");
              var tokenSplit = state.config.accessToken.split(".");
              var decodeBytes = atob(tokenSplit[1]);
              var parsead = JSON.parse(decodeBytes);
              var fechaUnix = parsead.exp;
              /*
               * Fecha formateada de unix a fecha normal
               * */
              var expiracion = new Date(fechaUnix * 1000);
              var hoy = new Date(); //fecha actual
              /*
               * Se obtiene el tiempo transcurrido en milisegundos
               * */
              var tiempoTranscurrido = expiracion - hoy;
              /*
               * Se obtienen las horas de diferencia a partir de la conversión de los
               * milisegundos a horas.
               * */
              var horasDiferencia =
                Math.round(
                  (tiempoTranscurrido / 3600000 + Number.EPSILON) * 100
                ) / 100;

              if (hoy > expiracion || horasDiferencia < state.tiempo_minimo) {
                state.sin_token = "expirado";
              }
            }
          }
        } catch (error2) {
          console.log(error2);
        }
      },
      saveDataHubspot:function(state){
        //   console.log("Entra al hubs")
          //  const socios ={
          //    "MIGO" : "MIGO SEGUROS"
          //  }
           let data ={
             mobilephone: state.formData.telefono, 
             email: state.formData.correo,
             firstname: state.formData.nombre,
             lastname: "",
             marca: state.formData.marca,
             modelo: state.formData.modelo,
             submarca:state.formData.descripcion,
             socio: state.config.socioHubspot,
          //  socio: socios[state.config.aseguradora]
           };
   
         //  console.log("data ", data)
             saveDataHubspot
             .prueba(
             JSON.stringify(data)
             )
             .then((resp) => {
             state.formData.idHubspot = resp.data.id;
             this.commit("saveDataCliente");
             //sube arriba de customer
            // this.saveDataCliente();
             
             })
             .catch((error) => {
             console.log("mensaje de error33 ¨*********",error )
                     var status = "ERR_CONNECTION_REFUSED";
                     if (typeof error.status != "undefined") {
                       status = error.status;
                     } else if (typeof error.response != "undefined") {
                       if (typeof error.response.status != "undefined") {
                         status = error.response.status;
                       }
                     }
                     if (status === 401) {
                       this.$store
                         .dispatch("getToken")
                         .then((resp) => {
                           if (typeof resp.data == "undefined") {
                             state.config.accessToken =
                               resp.accessToken;
                           } else if (
                             typeof resp.data.accessToken !=
                             "undefined"
                           ) {
                   state.formData.idHubspot = resp.data.message
                   console.log("mensaje de error11 ¨*********",state.formData.idHubspot )
                           }
                          // this.saveDataCliente();
                           
                         })
                         .catch((error) => {
                           
                 state.formData.idHubspot = resp.data.message
                         //  this.saveDataCliente();
                         });
                     } else {
               state.formData.idHubspot = typeof error.response == 'undefined' ? "{}" : error.response.data.message
                      
                      this.commit("saveDataCliente")
                     }
                   });
         },
     
      validateData: function(state) {
        
      setTimeout(this.commit("saveDataHubspot"),5000)
        state.formData.clave=state.formData.clave.toString();
        let dataCotizacion = {
          aseguradora: state.config.aseguradora,
          clave: state.formData.clave,
          cp: state.formData.cp,
          descripcion: state.formData.descripcion,
          descuento: state.config.descuento,
          edad: state.formData.edad,
          fechaNacimiento: (
            "01/01/" +
            (new Date().getFullYear() - state.formData.edad).toString()
          ).toString("yyyyMM/dd"),
          genero: state.formData.genero === "M" ? "MASCULINO" : "FEMENINO",
          marca: state.formData.marca,
          modelo: state.formData.modelo,
          movimiento: "cotizacion",
          paquete: "AMPLIA",
          servicio: "PARTICULAR",
        };
        state.config.dataPeticionCotizacion = dataCotizacion;
        console.log(state.config.dataPeticionCotizacion);
        console.log("validate Data");

        validacion
          .validarCorreo(state.formData.correo)
          .then((resp) => {
            state.formData.emailValid = resp.data.valido;
            validacion
              .validarTelefono(state.formData.telefono)
              .then((resp) => {
                state.formData.telefonoValid = resp.data.mensaje;
                this.commit("validacionCP");
              })
              .catch((error) => {
                state.formData.telefonoValid = "FIJO";
                state.idEndPoint = 169;
                state.status =
                  error.response !== undefined ? error.response.status : 500;
                state.message =
                  error.response !== undefined
                    ? JSON.stringify({
                        error: "error en el servicio: " + error.response,
                        peticion: state.formData.telefono,
                      })
                    : JSON.stringify({
                        error: "error en el servicio: " + error,
                        peticion: state.formData.telefono,
                      });
            
                this.commit("validacionCP");
              });
          })
          .catch((error) => {
            state.formData.emailValid = true;
            state.idEndPoint = 157;
            state.status =
              error.response !== undefined ? error.response.status : 500;
            state.message =
              error.response !== undefined
                ? JSON.stringify({
                    error: "error en el servicio: " + error.response,
                    peticion: state.formData.correo,
                  })
                : JSON.stringify({
                    error: "error en el servicio: " + error,
                    peticion: state.formData.correo,
                  });
        
            validacion
              .validarTelefono(state.formData.telefono)
              .then((resp) => {
                state.formData.telefonoValid = resp.data.mensaje;
                this.commit("validacionCP");
              })
              .catch((error) => {
                state.formData.telefonoValid = "FIJO";
                state.idEndPoint = 169;
                state.status =
                  error.response !== undefined ? error.response.status : 500;
                state.message =
                  error.response !== undefined
                    ? JSON.stringify({
                        error: "error en el servicio: " + error,
                        peticion: state.formData.telefono,
                      })
                    : JSON.stringify({
                        error: "error en el servicio: " + error,
                        peticion: state.formData.telefono,
                      });
            
                this.commit("validacionCP");
              });
          });
      },
      validacionCP(state) {
        validacion
          .validarCodigoPostal(
            state.formData.cp,
            state.config.accessToken
          )
          .then((resp) => {
            state.formData.codigoPostalValid =
              typeof resp.data == "object" && resp.data.length > 0
                ? true
                : false;
            // this.commit("saveData");
          })
          .catch((error) => {
            console.log(error);
            var status = error.status
              ? error.status
              : error.response.status
              ? error.response.status
              : "ERR_CONNECTION_REFUSED";
            state.formData.codigoPostalValid = true;
            if (status === 401) {
              this.dispatch("getToken")
                .then((resp) => {
                  state.config.accessToken = resp.data.accessToken;
                 // this.commit("validacionCP");
                })
                .catch((error) => {
                  this.commit("saveData");
              
                });
            } else {
              state.idEndPoint = 76;
              state.status =
                status !== "ERR_CONNECTION_REFUSED"
                  ? error.response.status
                  : 500;
              state.message =
                status !== "ERR_CONNECTION_REFUSED"
                  ? JSON.stringify({
                      error: error.response,
                      peticion: JSON.parse(error.config.data),
                    })
                  : JSON.stringify({
                      error: error,
                      peticion: state.formData.cp,
                    });
          
              // this.commit("saveData");
            }
          });
      },
      saveData: function(state) {
        state.formData.grupoCallback = state.config.grupoCallback;
        state.formData.dominioCorreo = state.config.dominioCorreo;
        database
          .search(
            state.formData.nombre,
            state.formData.telefono,
            state.formData.correo,
            state.formData.genero,
            state.formData.cp,
            state.formData.edad,
            JSON.stringify(state.formData),
            state.config.cotizacion === true
              ? JSON.stringify(state.cotizacion)
              : "{}",
            state.formData.emailValid == true ? 1 : 0,
            state.formData.telefonoValid,
            state.config.textoSMS,
            state.formData.codigoPostalValid == true ? 1 : 0,
            state.config.idSubRamo,
            state.config.idMedioDifusion,
            state.formData.aseguradora,
            state.config.idPagina,
            state.config.ipCliente,
            state.config.telefonoAS,
            state.config.accessToken
          )
          .then((resp) => {
            state.ejecutivo.nombre = resp.data.nombreEjecutivo;
            state.ejecutivo.correo = resp.data.correoEjecutivo;
            this.commit("valid");
            state.cargandocotizacion = true;
          })
          .catch((error) => {
            this.commit("valid");
            console.log("error saveData", error);
            var status =
              error.response !== undefined
                ? error.response.status
                : "ERR_CONNECTION_REFUSED";
            if (status === 401) {
              this.dispatch("getToken")
                .then((resp) => {
                  state.config.accessToken = resp.data.accessToken;
                  this.commit("saveData");
                })
                .catch((error) => {
                  var status =
                    error.response !== undefined
                      ? error.response.status
                      : "ERR_CONNECTION_REFUSED";
                  state.idEndPoint = 155;
                  state.status =
                    status !== "ERR_CONNECTION_REFUSED" ? status : 500;
                  state.message =
                    status !== "ERR_CONNECTION_REFUSED"
                      ? JSON.stringify({
                          error: error.response.data,
                          peticion: JSON.parse(error.config.data),
                        })
                      : "ERR_CONNECTION_REFUSED";
              
                });
            } else {
              state.idEndPoint = 84;
              state.status = status !== "ERR_CONNECTION_REFUSED" ? status : 500;
              state.message =
                status !== "ERR_CONNECTION_REFUSED"
                  ? JSON.stringify({
                      error: error.response.data,
                      peticion: JSON.parse(error.config.data),
                    })
                  : "ERR_CONNECTION_REFUSED";
          
            }
          });
         delete(state.formData.urlOrigen);
      },

      valid: function(state) {
        console.log("Validando datos de la cotizacion");
        if (state.formData.precio === "" && state.ejecutivo.id === "") {
          state.msj = true;
        }

        if (
          state.formData.precio === 0.0 ||
          state.formData.precio === "" ||
          state.formData.precio < 1000
        ) {
          state.msjEje = true;
          state.cargandocotizacion = true;
        } else {
          state.cargandocotizacion = true;
        }
      },
      limpiarCoberturas(state) {
        if (state.cotizacion.Coberturas) {
          for (var c in state.cotizacion.Coberturas[0]) {
            if (state.cotizacion.Coberturas[0][c] === "-") {
              delete state.cotizacion.Coberturas[0][c];
            }
          }
        }
      },
    },
  });
};
export default createStore;
