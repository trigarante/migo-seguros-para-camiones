module.exports = {
  /*
   ** Headers of the page
   */
  head: {
    title: "migoseguros.com",
    meta: [
      { charset: "utf-8" },
      { hid: "robots", name: "robots", content: "index, follow" },
      { name: "viewport", content: "width=device-width, initial-scale=1" },
      { hid: "description", name: "description", content: "Migo Seguros" },
    ],
    link: [{ rel: "icon", type: "image/png", href: "/favicon.png" }],
  },
  /*
   ** Customize the progress bar color
   */
  loading: { color: "#3bd600" },
  /*
   ** Build configuration
   */
  build: {
    /*
     ** Run ESLint on save
     */
    extend(config, { isDev, isClient }) {
      if (isDev && isClient) {
        config.module.rules.push({
          enforce: "pre",
          test: /\.(js|vue)$/,
          // loader: "eslint-loader",
          exclude: /(node_modules)/,
        });
      }
    },
  },
  router: {
    base: "/seguros-para-camiones/",
  },
  // include bootstrap css
  css: ["static/css/bootstrap.min.css", "static/css/styles.css"],
  plugins: [
    { src: "~/plugins/filters.js", ssr: false }, { src: '~/plugins/apm-rum.js', ssr: false },
  ],
  modules: [

  ],
  env: {

    /*PRUEBAS*/
    newCore: "https://dev.core-brandingservice.com",

    Environment:'DEVELOP',

    /**VALIDACIONES */
    urlValidaciones: "https://core-blacklist-service.com/rest", //PRODUCCIÓN

    /**CONFIGURACION*/
    urlGetConfiguracionCRM: "https://www.mark-43.net/mark43-service/v1", //PRODUCCIÓ
    /* CAMIONES */
    urlTactos: "https://ws-camiones.com", 
    promoCore: "https://dev.core-persistance-service.com", //CORE DESCUENTOS
    hubspot:"https://core-hubspot-dev.mark-43.net/deals/landing",
  },
  render: {
    http2: { push: true },
    resourceHints: false,
    gzip: { threshold: 9 },
  },
};
